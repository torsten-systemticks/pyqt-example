FROM gitpod/workspace-full-vnc

USER gitpod

RUN sudo apt-get update \
    && sudo apt-get install -y python3-pyqt5 \
    && sudo apt-get install -y pyqt5-dev-tools